﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2048v2
{
    public delegate void DeShow(int x, int y, int score);
    public delegate void ShowStat(int balls);
    public partial class Form1 : Form
    {
        static int size = 4;
        Label[,] labels;
        Dictionary<int, Color> backColors;
        Logic logic;

        public Form1()
        {
            InitializeComponent();
            initBackColors();
            initLabels();
            logic = new Logic(size , Show, ShowStat);
            logic.initGame();
        }
        private void initBackColors() //Kolory dla poszczególnych liczb
        {
            backColors = new Dictionary<int, Color>();
            backColors.Add(0, Color.FromArgb(215, 215, 215));
            backColors.Add(2, Color.FromArgb(237, 247, 248));
            backColors.Add(4, Color.FromArgb(228, 251, 227));
            backColors.Add(8, Color.FromArgb(241, 223, 122));
            backColors.Add(16, Color.FromArgb(241, 186, 103));
            backColors.Add(32, Color.FromArgb(236, 138, 79));
            backColors.Add(64, Color.FromArgb(239, 85, 32));
            backColors.Add(128, Color.FromArgb(239, 245, 120));
            backColors.Add(256, Color.FromArgb(250, 250, 52));
            backColors.Add(512, Color.FromArgb(242, 181, 233));
            backColors.Add(1024, Color.FromArgb(242, 181, 233));
            backColors.Add(2048, Color.FromArgb(242, 181, 233));
            backColors.Add(4096, Color.FromArgb(242, 181, 233));
            backColors.Add(8192, Color.FromArgb(242, 181, 233));
            backColors.Add(16384, Color.FromArgb(242, 181, 233));
            backColors.Add(32768, Color.FromArgb(242, 181, 233));
            backColors.Add(65536, Color.FromArgb(242, 181, 233));
        }
        private void initLabels() //Zainicjalizowanie poszczególnych label'ów
        {
            int w = panel1.Width / size;
            int h = panel1.Height / size;
            labels = new Label[size, size];
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    labels[x, y] = createLabel();
                    labels[x, y].Size = new System.Drawing.Size(w - 10, h - 10);
                    labels[x, y].Location = new Point(x * w + 10, y * h + 10);
                    panel1.Controls.Add(labels[x, y]);
                }
            }
        }
        private Label createLabel() // Tworzenie label'a - nadawanie koloru tła , czcionki itd
        {
            Label label = new Label();
            label.BackColor = System.Drawing.Color.LightCoral;
            label.Font = new System.Drawing.Font("Tahoma", 16.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label.Location = new System.Drawing.Point(0, 0);
            label.Text = "-";
            label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            return label;
        }
        public void Show(int x, int y, int score) // Pokazywanie odpowiedniego wyniku w komórce i nadawanie koloru odpowiednim komórką
        {
            labels[x, y].Text = score == 0 ? "" : score.ToString();
            labels[x, y].BackColor = backColors[score];
        }
        private void ShowStat(int balls) // funkcja do pokazywania wyniku gracza , zmienna balls przechowuje wage komórek które się połączyły i na tej podstawie jest obliczany wynik np. jeżeli połączyły się dwójki to balls wynosi 4
        { 
            try
            {
                int score = Convert.ToInt32(Score.Text);
                string str = Convert.ToString(score + balls);
                Score.Text = str;
            }
            catch (Exception)
            {
                MessageBox.Show("Error, the score must be a number");
            }
        }
        

        private void Form1_KeyDown(object sender, KeyEventArgs e) // zdarzenie przyciśnięcia przyscisku , w zależności od przycisku wybierana jest odpowiednia funkcja
        {
            switch (e.KeyCode)
            {
                case Keys.Left: logic.shiftLeft(); break;
                case Keys.Right: logic.shiftRight(); break;
                case Keys.Up: logic.shiftUp(); break;
                case Keys.Down: logic.shiftDown(); break;
                case Keys.Escape: logic.initGame(); break;
                default: break;
            }
            if (logic.gameOver()) // jeżeli będą spełnione warunki dla zakończonej gry wyświetli się komunikat z wynikiem
            {
                MessageBox.Show("Game Over, your score is: " + Score.Text);
            }
        }
    }

}
